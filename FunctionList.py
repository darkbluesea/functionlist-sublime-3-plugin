import sublime, sublime_plugin

function_list_view_id = False

def set_function_list_view_id(id):
    global function_list_view_id
    function_list_view_id = id

def get_function_list_view_id():
    return function_list_view_id

def check_ativated():
  s = sublime.load_settings("FunctionList.sublime-settings")
  return s.get("activated", False)

def set_activated(new_status):
    s = sublime.load_settings("FunctionList.sublime-settings")
    s.set("activated", new_status)
    sublime.save_settings("FunctionList.sublime-settings")    
    if check_ativated():
        print ("FunctionList Plugin is ON" )
    else:
        print ("FunctionList Plugin is OFF" )

class ActivateFunctionList(sublime_plugin.ApplicationCommand):
    def run (self):      

        set_activated( not check_ativated() ) # toggle
        sublime.active_window().run_command('function_list')

class FunctionList(sublime_plugin.WindowCommand):
    symbol_view = None
    symbol_pane = None

    def run(self):
        if not check_ativated():            
            return None

        active_view = self.window.active_view()
        active_group_index = self.window.active_group()

        output = self.get_view_symbols_as_text(active_view)

        if get_function_list_view_id() is False:
            self.destroy_references()

        view = self.get_symbol_view()

        self.window.focus_group(active_group_index)
        self.window.focus_view(active_view)
        
        view.set_syntax_file('Packages/FunctionList/FunctionList.tmLanguage')
        view.run_command('clear_text')
        view.run_command('write_text', {'text': output})
        view.run_command('write_text', {'text': self.get_intro_text()})


    def get_view_symbols_as_text(self, view):
        output = ''
        for (region, symbol) in view.symbols():
            row_col = view.rowcol(region.begin())
            row = str(row_col[0] + 1) # starts at 0
            output += row + ": " + symbol + "\n"
        return output

    def get_intro_text(self):
        return "Symbols List\n" + "line: symbol name\n\n"

    def get_symbol_view(self):
        if self.symbol_view is None:
            self.symbol_view = self.window.new_file()
            set_function_list_view_id ( self.symbol_view.id() )
            symbol_pane = self.get_symbol_pane()

        return self.symbol_view

    def get_symbol_pane(self):
        if self.symbol_pane is None:
            self.symbol_pane = self.window.run_command('new_pane', {'move': 1})

        return self.symbol_pane

    def destroy_references(self):
        self.symbol_pane = None
        self.symbol_view = None

        
class WriteText(sublime_plugin.TextCommand):
    def run(self, edit, text):
        self.view.insert(edit, 0, text)


class ClearText(sublime_plugin.TextCommand):
    def run(self, edit):
        while not self.view.visible_region().empty():
            self.view.erase(edit, self.view.visible_region())


class UpdateViewOnModified(sublime_plugin.EventListener):
    def on_modified(self, view):
        if check_ativated():
            # File modified, but is the current view? ex: third party file changers like git
            if view == view.window().active_view():
                view.window().run_command('function_list')  

class UpdateViewOnfocus(sublime_plugin.EventListener):
    def on_activated(self, view):
        if check_ativated():
            if function_list_view_id is not False and view.id() != function_list_view_id:
                view.window().run_command('function_list')  

class ClearReferences(sublime_plugin.EventListener):
    def on_close(self, view):
        if view.id() == function_list_view_id:
            set_function_list_view_id(False)
            set_activated(False)

